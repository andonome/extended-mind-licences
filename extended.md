---
theme:
	Warsaw
---

# Extended Mind Theory

- Parts of our minds are in objects
- No literally

**

![Chalmers](chalmers.jpg)

# Licensing

- Minds can be made partially of software.
- All software can come under a licence.
- Therefore parts of minds can come under a licence.

# Auditing

- Some software cannot be audited.
- Therefore parts of of a mind might be immune to auditing.

# Robocop 2

- Robocop is licensed to OCP.
- Control over a part of his mind accords full control over his actions.
- So OCP controls Robocop, due to their licences

# Terms and Conditions

![](roboco_directives.png)

# Real World Robocop

![](updates_mofo.jpg)

# Auditing

- Some software cannot be audited.
- Therefore parts of of a mind might be immune to auditing.

# Nosedive

- The main character loves having a high social rating
- We see repeatedly that nobody can audit why someone has their social rating properly.
   * Her friend just sees her social credit rating, not the reason for it.
   * She sees a truck driver, but not the reason for the truck driver's low social credit rating.

# Real World Nosedive

- Twitter does not fully explain why some people are 'verified' and why others are not.
- Try swearing in Google translate.
- Google: 'Moon Landing Hoax'.

